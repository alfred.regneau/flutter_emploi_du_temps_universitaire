# Application flutter - Emploi du temps universitaire

Une application mobile/multi-platformes pour visualiser son emploi du temps dans les universités Orléans-Tours ou via URL iCalendar.

## Pour ajouter une langue à l'application :

- Ajouter/Modifier un fichier ```"lang.json"``` dans ```assets/translations/```
- Lancer ces 2 commandes :
 ```
- flutter pub run easy_localization:generate -S assets/translations/ -O lib/translations  
- flutter pub run easy_localization:generate -S assets/translations -O lib/translations/ -o locale_keys.g.dart -f keys
```
