import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:emploi_du_temps_universitaire/translations/locale_keys.g.dart';
import 'package:shared_preferences/shared_preferences.dart';


class PreferencesModele{
  late SharedPreferences prefs ;
  static late PreferencesModele? _preferencesModele = null;

  PreferencesModele._();

  static Future<PreferencesModele?> getInstance() async{
    if(_preferencesModele == null){
      _preferencesModele = PreferencesModele._();
      _preferencesModele?.prefs = (await _preferencesModele?.getSp())!;
        return _preferencesModele ;

    }
    else{
      return _preferencesModele;
    }
  }



  Future<SharedPreferences> getSp() async{
    return await SharedPreferences.getInstance();
  }

  List<CalendarPreference> getAllConfig(){
    var listStr = prefs.getStringList("configs");

    List<CalendarPreference> res = [];
    if(listStr == null){
      return res;
    }
    for(String strConfig in listStr){
      res.add(CalendarPreference.fromJson(jsonDecode(strConfig)));
    }
    return res;
  }

  bool addConfig(CalendarPreference pref){
    String prefStr = jsonEncode(pref.toJson());
    var listStr = prefs.getStringList("configs");
    if(listStr ==null){
      prefs.setStringList("configs", [prefStr]);
      setSelectedPref(pref);
    }
    else{
      if(alreadyInPref(pref.url)){
        return false;
      }
      listStr.add(prefStr);
      prefs.setStringList("configs", listStr);
    }
    return true;
  }

  deleteConfig(int numConfig){
    var listStr = prefs.getStringList("configs");
    if(listStr==null){
      return;
    }
    var sizeList = listStr.length;
    if(sizeList<numConfig){
      return;
    }
    listStr.removeAt(numConfig);
    prefs.setStringList("configs", listStr);
  }

  bool alreadyInPref(String url){
    for(CalendarPreference elem in getAllConfig()){
      if(elem.url == url){
        return true;
      }
    }
    return false;
  }

  CalendarPreference? getSelectedPref(){
    return prefs.getString("selectedPref") != null ? CalendarPreference.fromJson(jsonDecode(prefs.getString("selectedPref")!)) : null;
  }

  void setSelectedPref(CalendarPreference selected){
    prefs.setString("selectedPref", jsonEncode(selected.toJson()));
  }

  void deleteAllConfig() {
    prefs.clear();
  }


}

enum CalendarType {
  icalendarUrl,
  studentNumber,
  componentDegree
}

class CalendarPreference{
  CalendarType type;
  String title = "";
  String studentNumber = "";
  String component = "";
  String degree = "";
  String url = "";


  CalendarPreference({
    required this.type,
    required this.studentNumber,
    required this.component,
    required this.degree,
    required this.title,
    required this.url
  });


  CalendarPreference.withUrl(this.type,this.title,this.url);
  
  CalendarPreference.withNum(this.type,this.title,this.studentNumber,this.url);

  CalendarPreference.withDegree(this.type,this.title,this.component,this.degree,this.url);

  factory CalendarPreference.fromJson(Map<String, dynamic> parsedJson) {
    return CalendarPreference(
        type: CalendarType.values[parsedJson['type']],
        title: parsedJson['title'] ?? "",
        studentNumber: parsedJson['studentNumber'] ?? "",
        component: parsedJson['component'] ?? "",
        degree: parsedJson['degree'] ?? "",
        url: parsedJson['url'] ?? ""
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "type": type.index,
      "title": title,
      "studentNumber": studentNumber,
      "component": component,
      "degree": degree,
      "url": url,
    };
  }

  @override
  String toString(){
    switch(type) {
      case CalendarType.icalendarUrl:
        return LocaleKeys.select_calendar_type.tr() + LocaleKeys.config_icalendar_url.tr();
      case CalendarType.studentNumber:
        return LocaleKeys.select_calendar_current_num_full.tr() + studentNumber + "\n";
      case CalendarType.componentDegree:
        return LocaleKeys.select_calendar_component.tr() + component + "\n" +
               LocaleKeys.select_calendar_degree.tr() + degree + "\n";
      default:
        return "";
    }
  }
}