import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:icalendar_parser/icalendar_parser.dart';

class ICalendarEventModel {

  DateTime _dateStamp;
  DateTime _dateStart;
  DateTime _dateEnd;
  String _summary;
  String _location;
  String _description;
  String _uid;
  int _sequence;
  List<Object> resourceIds;

  ICalendarEventModel({
    required DateTime dateStamp,
    required DateTime dateStart,
    required DateTime dateEnd,
    required String summary,
    required String location,
    required String description,
    required String uid,
    required int sequence}) :
        _dateStamp = dateStamp,
        _dateStart = dateStart,
        _dateEnd = dateEnd,
        _summary = summary,
        _location = location,
        _description = description,
        _uid = uid,
        _sequence = sequence,
        resourceIds = [];

  factory ICalendarEventModel.fromICal(Map<String, dynamic> event) {
    return ICalendarEventModel(
      dateStamp: (event["dtstamp"] as IcsDateTime).toDateTime()!.add(const Duration(hours: 2)),
      dateStart: (event["dtstart"] as IcsDateTime).toDateTime()!.add(const Duration(hours: 2)),
      dateEnd: (event["dtend"] as IcsDateTime).toDateTime()!.add(const Duration(hours: 2)),
      summary: event["summary"].trim().replaceAll("\\n", "\n"),
      location: event["location"].trim().replaceAll("\\n", "\n"),
      description: event["description"].trim().replaceAll("\\n", "\n"),
      uid: event["uid"],
      sequence: int.tryParse(event["sequence"] ?? "0") ?? 0,
  );
  }

  set dateStamp(DateTime dateStamp) => _dateStamp = dateStamp;
  set dateStart(DateTime dateStart) => _dateStart = dateStart;
  set dateEnd(DateTime dateEnd) => _dateEnd = dateEnd;
  set summary(String summary) => _summary = summary;
  set location(String location) => _location = location;
  set description(String dateStart) => _description = description;
  set uid(String uid) => _uid = uid;
  set sequence(int sequence) => _sequence = sequence;


  DateTime get dateStamp => _dateStamp;

  DateTime get dateStart => _dateStart;

  DateTime get dateEnd => _dateEnd;

  String get summary => _summary.startsWith("\n") ? _summary.substring(1, _summary.length) : _summary;

  String get location => _location.startsWith("\n") ? _location.substring(1, _location.length) : _location;

  String get description => _description.startsWith("\n") ? _description.substring(1, _description.length) : _description;


  String dayStart(Locale locale) {
    String day = DateFormat.yMMMMEEEEd(locale.languageCode).format(_dateStart);
    return "${day[0].toUpperCase()}${day.substring(1)}";
  }

  String get hourStart => DateFormat.jm().format(_dateStart);

  String get duration {
    Duration duration = _dateEnd.difference(_dateStart);
    return DateFormat.Hm().format(DateTime.fromMicrosecondsSinceEpoch(duration.inMicroseconds, isUtc: true));
  }



  String get uid => _uid;

  int get sequence => _sequence;

  @override
  String toString() {
    return _summary;
  }


}