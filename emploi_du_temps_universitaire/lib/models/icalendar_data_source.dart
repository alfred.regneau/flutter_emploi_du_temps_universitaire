import 'package:emploi_du_temps_universitaire/models/icalendar_event_model.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class ICalendarDataSource extends CalendarDataSource {

  ICalendarDataSource(List<ICalendarEventModel> calendarEvents, List<CalendarResource>? resourceColl) {
    appointments = calendarEvents;
    resources = resourceColl;
  }

  @override
  DateTime getStartTime(int index) {
    return _getCalendarEventModel(index).dateStart;
  }

  @override
  DateTime getEndTime(int index) {
    return _getCalendarEventModel(index).dateEnd;
  }

  @override
  String getSubject(int index) {
    return _getCalendarEventModel(index).summary + "\n" +
        _getCalendarEventModel(index).location + "\n" +
        _getCalendarEventModel(index).description;
  }

  @override
  Color getColor(int index) {
    int sequence = _getCalendarEventModel(index).sequence;
    int n = 40;
    int r = sequence % (256 - n * 2) + n;
    int g = ((sequence / 10) % (256 - n * 2) + n).toInt();
    int b = ((sequence / 100) % (256 - n * 2) + n).toInt();
    return Color.fromARGB(255, r, g, b);
  }

  @override
  List<Object> getResourceIds(int index) {
    return _getCalendarEventModel(index).resourceIds;
  }

  ICalendarEventModel _getCalendarEventModel(int index) {
    final dynamic event = appointments![index];
    late final ICalendarEventModel eventData;
    if (event is ICalendarEventModel) {
      eventData = event;
    }
    return eventData;
  }
}