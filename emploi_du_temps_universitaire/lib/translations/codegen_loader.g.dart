// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> fr = {
  "test_fetch_event": "Test récupération event",
  "title": {
    "home": "Accueil",
    "event": "Événement",
    "config": "Configuration",
    "select": "Selection",
    "calendar": "Emploi du temps",
    "calendar_compare": "Comparaison EDT",
    "language": "Langue"
  },
  "button": {
    "cancel": "Annuler",
    "validate": "Valider",
    "ok": "OK",
    "reset": "Réinitialiser"
  },
  "event": {
    "summary": "Matière",
    "location": "Localisation",
    "day_start": "Jour",
    "hour": "Heure",
    "duration": "Durée",
    "description": "Description",
    "classroom": "Salle",
    "slots_list": "Liste des créneaux"
  },
  "question": {
    "delete_pref": "Êtes-vous sûr de vouloir supprimer cette configuration ?",
    "delete_all_prefs": "Êtes-vous sûr de vouloir réinitialiser toutes les configurations ?"
  },
  "error": {
    "no_connection": "La connexion a échoué",
    "invalid_format": "Format invalide",
    "unknown": "Erreur",
    "status_code": "Erreur, Status code : {code}",
    "bad_request": "Mauvaise requête",
    "unauthorized": "Requête non autorisée",
    "bad_student_number": "Numéro Incorrect",
    "bad_student_number_desc": "Le numéro renseigné ne correspond a aucun étudiant",
    "no_degree_select_desc": "Veuillez selectionné votre composante et filière avant de valider",
    "config_already_register": "Les informations renseignés sont déjà dans la base de données",
    "calendar_loading_title": "Erreur de chargement",
    "calendar_loading_desc": "Pas de calendrier selectionné",
    "no_config_saved": "Aucune configuration enregistré",
    "add_config_with_menu": "Ajoutez en une via le menu en haut à droite",
    "no_other_config_saved": "Aucune autre configuration",
    "add_config_with_config": "Ajoutez en une via la page de configuration",
    "invalid_ical_config": "Lien ICalendar invalide",
    "invalid_ical_config_msg": "La configuration sélectionnée ne contient pas un format iCalendar valide"
  },
  "select_calendar": {
    "no_text_error": "Veuillez entrer un numero",
    "current_num_empty": "Entrez votre numero :",
    "current_num_full": "Numéro enregistré : ",
    "title": "Titre : ",
    "type": "Type : ",
    "component": "Composante : ",
    "degree": "Fillière : ",
    "delete_button": "Supprimer",
    "prefs_title": " Preferences : ",
    "config_added": "Configuration ajoutée"
  },
  "config": {
    "hint": {
      "title": "Exemple 1",
      "url": "http://www.exemple.fr/",
      "student_number": "1234567"
    },
    "error": {
      "enter_title": "Veuillez entrer un titre valide",
      "enter_url": "Veuillez entrer une url valide",
      "enter_number": "Veuillez entrer numéro valide"
    },
    "title": {
      "add_config": "Ajouter une configuration",
      "university_orleans_tours": "Universités Orléans-Tours",
      "others_configs": "Autres configurations"
    },
    "calendar_title": "Titre",
    "icalendar_url": "Lien ICalendar",
    "student_number": "Numéro étudiant",
    "component_degree": "Composante/Filière",
    "language": {
      "english": "Anglais",
      "french": "Francais",
      "change_language": "L'application est maintenant en Francais"
    }
  }
};
static const Map<String,dynamic> en = {
  "test_fetch_event": "Test récupération event",
  "title": {
    "home": "Home",
    "event": "Event",
    "config": "Configuration",
    "select": "Selection",
    "calendar": "Schedule",
    "calendar_compare": "Compare Schedule",
    "language": "Language"
  },
  "button": {
    "cancel": "Cancel",
    "validate": "Validate",
    "ok": "OK",
    "reset": "Reset"
  },
  "event": {
    "summary": "Summary",
    "location": "Location",
    "day_start": "Day",
    "hour": "Hour",
    "duration": "Duration",
    "description": "Description",
    "classroom": "Classroom",
    "slots_list": "Slots list"
  },
  "question": {
    "delete_pref": "Do you really want to reset this configuration ?",
    "delete_all_prefs": "Do you really want to reset all configurations ?"
  },
  "error": {
    "no_connection": "Connexion failed",
    "invalid_format": "Invalid format",
    "unknown": "Unknown",
    "status_code": "Error, Status code : {code}",
    "bad_request": "Bad request",
    "unauthorized": "Unauthorized request",
    "bad_student_number": "Bad student number",
    "bad_student_number_desc": "Number doesn't matches any student",
    "no_degree_select_desc": "Please select your component and degree before validate",
    "config_already_register": "The information is already in the database",
    "calendar_loading_title": "Loading error",
    "calendar_loading_desc": "No calendar select",
    "no_config_saved": "No configuration saved",
    "add_config_with_menu": "Add one with the menu at the top right",
    "no_other_config_saved": "No other configuration",
    "add_config_with_config": "Add one with the configuration page",
    "invalid_ical_config": "Invalid iCalendar URL",
    "invalid_ical_config_msg": "The selected configuration does not contain a valid iCalendar format"
  },
  "select_calendar": {
    "no_text_error": "Please enter a number",
    "current_num_empty": "Enter your number :",
    "current_num_full": "Student number : ",
    "title": "Title : ",
    "type": "Type : ",
    "component": "Component : ",
    "degree": "Degree : ",
    "delete_button": "Delete",
    "prefs_title": " Preferences : ",
    "config_added": "Configuration ajoutée"
  },
  "config": {
    "hint": {
      "title": "Exemple 1",
      "url": "http://www.exemple.fr/",
      "student_number": "1234567"
    },
    "error": {
      "enter_title": "Please enter a valid title",
      "enter_url": "Please enter a valid url",
      "enter_number": "Please enter a valid number"
    },
    "title": {
      "add_config": "Add a configuration",
      "university_orleans_tours": "College Orléans-Tours",
      "others_configs": "Others configurations"
    },
    "calendar_title": "Title",
    "icalendar_url": "ICalendar Link",
    "student_number": "Student number",
    "component_degree": "Component/Degree",
    "language": {
      "english": "English",
      "french": "French",
      "change_language": "The app is now in english"
    }
  }
};
static const Map<String, Map<String,dynamic>> mapLocales = {"fr": fr, "en": en};
}
