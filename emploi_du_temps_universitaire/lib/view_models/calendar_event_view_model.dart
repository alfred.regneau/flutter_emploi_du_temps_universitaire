import 'package:emploi_du_temps_universitaire/models/icalendar_event_model.dart';
import 'package:emploi_du_temps_universitaire/view_models/base_view_model.dart';

class CalendarEventViewModel extends BaseViewModel {

  late ICalendarEventModel _iCalendarEventModel;

  ICalendarEventModel get iCalendarEventModel => _iCalendarEventModel;

  setCalendarEvent(ICalendarEventModel iCalendarEventModel) async {
    _iCalendarEventModel = iCalendarEventModel;
  }

  //Récupère la liste des créneaux lié à un événement (c'est à dire la liste des événements identique à celui-ci)
  List<ICalendarEventModel> getIdenticalEvents(ICalendarEventModel event, List<ICalendarEventModel> events) {
    List<ICalendarEventModel> sameEventsSequence = [];
    for (ICalendarEventModel e in events) {
      if (event.sequence == e.sequence && event.summary == e.summary) {
        sameEventsSequence.add(e);
      }
    }
    return sameEventsSequence;
  }


}