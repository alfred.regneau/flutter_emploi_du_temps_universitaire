

import 'package:easy_localization/easy_localization.dart';
import 'package:emploi_du_temps_universitaire/models/preferences_model.dart';
import 'package:emploi_du_temps_universitaire/services/request_service.dart';
import 'package:emploi_du_temps_universitaire/translations/locale_keys.g.dart';
import 'package:emploi_du_temps_universitaire/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:icalendar_parser/icalendar_parser.dart';
import 'package:tuple/tuple.dart';

import '../services/result.dart';
import 'base_view_model.dart';

class ConfigsViewModel extends BaseViewModel {
  late PreferencesModele? spModele;
  late List<CalendarPreference> listPref;
  late CalendarPreference? selectedPref;

  List<Tuple2<String,String>> listComponent = [];
  List<Tuple2<String,String>> listDegree = [];
  Tuple2<String,String> actualComponent = const Tuple2("", "");
  Tuple2<String,String> acutalDegree= const Tuple2("", "");

  bool errorNum = false;
  bool alreadyIn = false;
  bool languageChange = false;

  
  gererErrorNum(){
    errorNum = false;
  }
  
  gererAlreadyIn(){
    alreadyIn = false;
  }

  initConfigs(){
    updateView(true);
    hasError = false;
     PreferencesModele.getInstance().then((value) {
       spModele = value;
       listPref = spModele!.getAllConfig();
       selectedPref = spModele!.getSelectedPref();
       updateView(false);
     });
  }

  acceptError(){
    updateView(true);
    hasError = false;
    updateView(false);
  }

  reloadPrefs(){
    updateView(true);
    listPref = spModele!.getAllConfig();
    selectedPref = spModele!.getSelectedPref();
    updateView(false);
  }

  setDegree(Tuple2<String,String> degree){
    updateView(true);
    acutalDegree = degree;
    updateView(false);
  }
  setComponent(Tuple2<String,String> component){
    actualComponent = component;
  }

  addPrefUrl(String title,String url){
    bool ok = spModele!.addConfig(CalendarPreference.withUrl(CalendarType.icalendarUrl,title, url));
    reloadPrefs();
    return ok;
  }

  addPrefNumber(String number,String url,String title){
    bool ok = spModele!.addConfig(CalendarPreference.withNum(CalendarType.studentNumber,title,number, url));
    reloadPrefs();
    return ok;
  }

  addPrefDegree(String component,String degree,String url,String title){
    bool ok = spModele!.addConfig(CalendarPreference.withDegree(CalendarType.componentDegree,title,component,degree, url));
    reloadPrefs();
    return ok;
  }

  deletePrefs(int num){
    spModele!.deleteConfig(num);
    reloadPrefs();
  }

  resetAllPrefs() {
    spModele!.deleteAllConfig();
    reloadPrefs();
  }

  selectConfig(CalendarPreference pref){
    spModele!.setSelectedPref(pref);
    reloadPrefs();
  }

  checkICalendarUrl(String title, String iCalUrl,BuildContext context) async{
    updateView(true);
    Result result = await RequestService.fetchICalendar(iCalUrl);
    if(result is Success){
      hasError = false;
      try {
        ICalendar.fromString(result.response.body, allowEmptyLine: true);
      } on ICalendarFormatException {
        hasError = true;
        error = Error(title: LocaleKeys.error_invalid_ical_config.tr(), description: LocaleKeys.error_invalid_ical_config_msg.tr());
        updateView(false);
        return;
      }

      bool ok = addPrefUrl(title, iCalUrl);
      if(!ok){
        alreadyIn = true;
      }
      else {
        initConfigs();
        showToast(FToast().init(context), LocaleKeys.select_calendar_config_added.tr(), Colors.green, const Icon(Icons.check));
        Navigator.pop(context);
      }

    }
    else if (result is Failure) {
      hasError = true;
      error = result.error;

    }
    updateView(false);

  }

  getTitleFromLink(String link) async{
    Result result = await RequestService.fetchICalendar(link);
    if (result is Success) {
      hasError = false;

      try {
        ICalendar.fromString(result.response.body, allowEmptyLine: true);
        var text = result.response.body;
        int start = text.indexOf("CN")+3;
        text = text.substring(start);
        int end = text.indexOf("BEGIN");
        if (start != -1 && end != -1) {
          text = text.substring(0,end);
          return text.substring(1,text.length-2);
        }
        return "";
      } on ICalendarFormatException {
        hasError = true;
        error = Error(title: LocaleKeys.error_invalid_ical_config.tr(), description: LocaleKeys.error_invalid_ical_config_msg.tr());
      }

    }
    else if (result is Failure) {
      hasError = true;
      error = result.error;
      return null;
    }

  }


  checkStudentNumber(String studentNumber,BuildContext context) async{
    updateView(true);
    Result result = await RequestService.validateStudentNumber(int.parse(studentNumber));
    if(result is Success){
      hasError = false;
      var resultBody = result.response.body;
      if(resultBody.contains("http://www.univ-orleans.fr/EDTWeb/export?project")) {
        var start = resultBody.indexOf(
            "http://www.univ-orleans.fr/EDTWeb/export?project");
        var end = resultBody.indexOf("&amp;type=ical") + "&amp;type=ical".length;

        var url = resultBody.substring(start, end).replaceAll("&amp;", "&");

        String? title = await getTitleFromLink(url);
        if(title!=null) {
          bool ok = addPrefNumber(studentNumber.toString(), url, title);

          if (!ok) {
            alreadyIn = true;
          }
          else {
            initConfigs();
            showToast(FToast().init(context), LocaleKeys.select_calendar_config_added.tr(), Colors.green, const Icon(Icons.check));
            Navigator.pop(context);
          }
        }
      }
      else{
        errorNum = true;
        }

    }
    else if (result is Failure) {
      hasError = true;
      error = result.error;

    }
    updateView(false);

  }

  getComponents() async{
    updateView(true);
    initConfigs();
    if(listComponent.isEmpty) {
      Result result = await RequestService.fetchComponents();
      if (result is Success) {
        var resultBody = result.response.body;
        resultBody = resultBody.replaceRange(
            0, resultBody.indexOf("<select name=\"composantes\""), "");

        resultBody =
            resultBody.replaceRange(0, resultBody.indexOf("<option"), "");
        resultBody = resultBody.replaceRange(
            resultBody.indexOf("</select>"), resultBody.length, "");

        RegExp reg = RegExp(r'"(.*?)"');
        var listCompHTML = resultBody.split("<option");
        listCompHTML.removeAt(0);
        for (String htmlComp in listCompHTML) {
          var matches = reg.allMatches(htmlComp);
          List<String> tmp = [];
          for (Match m in matches) {
            tmp.add(m[0]!);
          }
          listComponent.add(Tuple2(tmp[0].replaceAll("\"", ""), tmp[1].replaceAll("\"", "")));
        }
      }
    }
    updateView(false);
  }

  getDegrees(String component) async{
    updateView(true);
    listDegree.clear();

    Result result = await RequestService.fetchDegrees(component);
    if (result is Success) {
      var resultBody = result.response.body;
      resultBody = resultBody.replaceRange(
          0, resultBody.indexOf("<select name=\"filieres\""), "");
      resultBody =
          resultBody.replaceRange(0, resultBody.indexOf("<option"), "");
      resultBody = resultBody.replaceRange(
          resultBody.indexOf("</select>"), resultBody.length, "");

      RegExp reg = RegExp(r'"(.*?)"');
      var listDegreeHTML = resultBody.split("<option");
      listDegreeHTML.removeAt(0);

      for (String htmlDegree in listDegreeHTML) {
        var matches = reg.allMatches(htmlDegree);
        List<String> tmp = [];
        for (Match m in matches) {
          tmp.add(m[0]!);
        }
        listDegree.add(Tuple2(tmp[0].replaceAll("\"", ""), tmp[1].replaceAll("\"", "")));

      }
    }
    updateView(false);



  }

  validateDegree(BuildContext context) async {
    updateView(true);
    Result result = await RequestService.validateComponentDegree(actualComponent.item1, acutalDegree.item1);
    if(result is Success){
      hasError = false;
      var resultBody = result.response.body;
      if(resultBody.contains("http://www.univ-orleans.fr/EDTWeb/export?project")) {
        var start = resultBody.indexOf(
            "http://www.univ-orleans.fr/EDTWeb/export?project");
        var end = resultBody.indexOf("&amp;type=ical") + "&amp;type=ical".length;

        var url = resultBody.substring(start, end).replaceAll("&amp;", "&");

        String? title = await getTitleFromLink(url);
        if(title!=null) {
          bool ok = addPrefDegree(actualComponent.item2, acutalDegree.item2, url,title);
          if(!ok){
            alreadyIn = true;
          }
          else {
            initConfigs();
            showToast(FToast().init(context), LocaleKeys.select_calendar_config_added.tr(), Colors.green, const Icon(Icons.check));
            Navigator.pop(context);
          }
        }
      }
    }
    else if (result is Failure) {
      hasError = true;
      error = result.error;
    }
    updateView(false);
  }

  changeLanguage(BuildContext context,Locale lang){
    updateView(true);
    context.setLocale(lang);
    languageChange = true;
    updateView(false);
  }

}