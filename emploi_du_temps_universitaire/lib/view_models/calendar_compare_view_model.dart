import 'package:easy_localization/easy_localization.dart';
import 'package:emploi_du_temps_universitaire/models/icalendar_data_source.dart';
import 'package:emploi_du_temps_universitaire/models/icalendar_event_model.dart';
import 'package:emploi_du_temps_universitaire/services/request_service.dart';
import 'package:emploi_du_temps_universitaire/services/result.dart';
import 'package:emploi_du_temps_universitaire/translations/locale_keys.g.dart';
import 'package:flutter/material.dart';
import 'package:icalendar_parser/icalendar_parser.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

import '../models/preferences_model.dart';
import 'base_view_model.dart';

class CalendarCompareViewModel extends BaseViewModel {

  late ICalendarDataSource _iCalendarDataSource;
  late PreferencesModele? spModele;
  ICalendarDataSource get iCalendarDataSource => _iCalendarDataSource;
  late List<ICalendarEventModel> events;
  List<List<ICalendarEventModel>> eventsList = [];
  List<CalendarResource> resourceColl = [];
  int id = 0;

  initPrefs() {
    updateView(true);
    hasError = false;
    PreferencesModele.getInstance().then((value) {
      spModele = value;
      if (spModele!.getSelectedPref() == null){
        hasError = true;
        error = Error(title: LocaleKeys.error_calendar_loading_title.tr(), description: LocaleKeys.error_calendar_loading_desc.tr(),);
      }
      else{
        getCalendarEvent();
      }
    });
  }

  getCalendarEvent() async {
    eventsList.clear();
    resourceColl.clear();
    id=0;
    Result result = await RequestService.fetchICalendar(spModele!.getSelectedPref()!.url);
    if (result is Success) {
      hasError = false;
      ICalendar iCalendar = ICalendar.fromString(result.response.body, allowEmptyLine: true);
      events = List.generate(iCalendar.data.length, (index) => ICalendarEventModel.fromICal(iCalendar.data[index]));

      resourceColl.add(CalendarResource(
        displayName: spModele!.getSelectedPref()!.title.replaceAll(" ", "_"),
        id: id.toString(),
        color: Colors.white,
      ));
      id++;
      eventsList.add(List.from(events));
      _iCalendarDataSource = ICalendarDataSource(events, null);
    }
    else if (result is Failure) {
      hasError = true;
      error = result.error;
    }
    updateView(false);
  }

  /* Ajoute une nouvelle liste d'événements et un nouveau profil correspondants à un autre calendar*/
  addCalendar(CalendarPreference preference) async {
    updateView(true);
    Result result = await RequestService.fetchICalendar(preference.url);
    if (result is Success) {
      hasError = false;
      ICalendar iCalendar = ICalendar.fromString(
          result.response.body, allowEmptyLine: true);

      List<ICalendarEventModel> newEvents = List.generate(
          iCalendar.data.length, (index) =>
          ICalendarEventModel.fromICal(iCalendar.data[index]));
      eventsList.add(newEvents);
      resourceColl.add(CalendarResource(
        displayName: preference.title.replaceAll(" ", "_"),
        id: id.toString(),
        color: Colors.white,
      ));
      _fillResourceIds(eventsList);
      events.addAll(eventsList[id++]);
      _iCalendarDataSource = ICalendarDataSource(events, resourceColl);
    }
    else if (result is Failure) {
      hasError = true;
      error = result.error;
    }
    updateView(false);
  }

  void _fillResourceIds(List<List<ICalendarEventModel>> eventsList){
    for (int i = 0; i < eventsList.length; i++) {
      for (int j = 0; j < eventsList[i].length; j++){
        eventsList[i][j].resourceIds.add("$i");
      }
    }
  }
}