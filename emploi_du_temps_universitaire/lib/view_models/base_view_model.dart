import 'package:emploi_du_temps_universitaire/services/result.dart';
import 'package:flutter/material.dart';

class BaseViewModel extends ChangeNotifier {

  bool hasError = false;
  Error error = Error(title: "", description: "");

  bool _isLoading = false;
  bool get isLoading => _isLoading;

  // Met à jour l'affichage avec l'écran de chargement si isLoading = true, sinon affiche le contenu de la page
  updateView(bool isLoading) async {
    _isLoading = isLoading;
    notifyListeners();
  }

}