import 'package:easy_localization/easy_localization.dart';
import 'package:emploi_du_temps_universitaire/models/icalendar_data_source.dart';
import 'package:emploi_du_temps_universitaire/models/icalendar_event_model.dart';
import 'package:emploi_du_temps_universitaire/services/request_service.dart';
import 'package:emploi_du_temps_universitaire/services/result.dart';
import 'package:emploi_du_temps_universitaire/translations/locale_keys.g.dart';
import 'package:icalendar_parser/icalendar_parser.dart';

import '../models/preferences_model.dart';
import 'base_view_model.dart';

class CalendarViewModel extends BaseViewModel {

  late ICalendarDataSource _iCalendarDataSource;
  late PreferencesModele? spModele;

  ICalendarDataSource get iCalendarDataSource => _iCalendarDataSource;
  late List<ICalendarEventModel> events;



  initPrefs() async{
    updateView(true);
    hasError = false;
    spModele = await PreferencesModele.getInstance();
    if (spModele!.getSelectedPref() == null) {
      hasError = true;
      error = Error(title: LocaleKeys.error_calendar_loading_title.tr(),
      description: LocaleKeys.error_calendar_loading_desc.tr(),);
      updateView(false);
    }
    else {
      getCalendarEvents();
    }
  }

  getCalendarEvents() async {
    Result result = await RequestService.fetchICalendar(spModele!.getSelectedPref()!.url);
    if (result is Success) {
      hasError = false;
      try {
        ICalendar iCalendar = ICalendar.fromString(result.response.body, allowEmptyLine: true);
        events = List.generate(iCalendar.data.length, (index) =>
            ICalendarEventModel.fromICal(iCalendar.data[index]));
        _iCalendarDataSource = ICalendarDataSource(events, null);
      } on ICalendarFormatException {
        hasError = true;
        error = Error(title: LocaleKeys.error_invalid_ical_config.tr(), description: LocaleKeys.error_invalid_ical_config_msg.tr());
      }
    }
    else if (result is Failure) {
      hasError = true;
      error = result.error;
    }
    updateView(false);
  }


}