import 'package:easy_localization/easy_localization.dart';
import 'package:emploi_du_temps_universitaire/views/calendar_view.dart';
import 'package:emploi_du_temps_universitaire/views/configs/component_degree_view.dart';
import 'package:emploi_du_temps_universitaire/views/configs/icalendar_url_view.dart';
import 'package:emploi_du_temps_universitaire/views/configs/student_number_view.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

import '../components/app_loading.dart';
import '../models/preferences_model.dart';
import '../translations/locale_keys.g.dart';
import '../utils/navigation_utils.dart';
  import '../utils/utils.dart';
import '../view_models/calendar_view_model.dart';
import '../view_models/configs_view_model.dart';

class ConfigsListView extends StatelessWidget {
  const ConfigsListView({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    ConfigsViewModel configsViewModel = context.watch<ConfigsViewModel>();
    CalendarViewModel calendarViewModel = context.watch<CalendarViewModel>();

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      if(configsViewModel.languageChange && !configsViewModel.isLoading){
        showToast(FToast().init(context),LocaleKeys.config_language_change_language.tr(),Colors.greenAccent,const Icon(Icons.check));
        configsViewModel.languageChange = false;
      }
    });

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(LocaleKeys.title_config.tr()),
        actions: [
          PopupMenuButton<int>(
            onSelected: (value) {
              switch(value) {
                case 0:
                  _addConfigMenu(context, configsViewModel);
                  break;
                case 1:
                  _selectLanguageMenu(context,configsViewModel);
                  break;
                case 2:
                  _resetAllConfigs(context, configsViewModel);
                  break;
              }
            },
            itemBuilder: (context){
              return [
                PopupMenuItem<int>(
                  value: 0,
                  child: ListTile(
                    leading: const Icon(Icons.add),
                    title: Text(LocaleKeys.title_config.tr()),
                  ),
                ),
                PopupMenuItem<int>(
                    value: 1,
                    child: ListTile(
                      leading: const Icon(Icons.language),
                      title: Text(LocaleKeys.title_language.tr()),
                    ),
                ),
                const PopupMenuDivider(),
                PopupMenuItem<int>(
                    value: 2,
                    child: ListTile(
                      leading: const Icon(Icons.delete_forever, color: Colors.redAccent),
                      title: Text(LocaleKeys.button_reset.tr()),
                    )
                ),
              ];
            },
          ),
          IconButton(
            onPressed: () => _gotoCalendar(context, calendarViewModel),
            icon: const Icon(Icons.calendar_month),
          ),
        ],
      ),

      body: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Column(
          children: [
            // Text(LocaleKeys.select_calendar_prefs_title.tr(),
            //     style: const TextStyle(fontSize: 30.0,fontWeight: FontWeight.bold),
            // ),
            Flexible(child: _buildConfigsList(context, configsViewModel))
          ],
        )
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: (){
      //     _gotoCalendar(context,calendarViewModel);
      //   },
      //   child: const Icon(Icons.calendar_month),
      // )
    );
  }



  _addConfigMenu(BuildContext context, ConfigsViewModel configsViewModel) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(LocaleKeys.config_title_add_config.tr()),
        content:
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(LocaleKeys.config_title_university_orleans_tours.tr()),
            ListTile(
              textColor: Colors.lightBlue,
              leading: const Icon(Icons.school, color: Colors.lightBlue,),
              title: Text(LocaleKeys.config_student_number.tr()),
              onTap: () => {
                Navigator.pop(context),
                configsViewModel.initConfigs(),
                push(context, const StudentNumberView())
              },
            ),
            ListTile(
              textColor: Colors.lightBlue,
              leading: const Icon(Icons.book, color: Colors.lightBlue),
              title: Text(LocaleKeys.config_component_degree.tr()),
              onTap: () => {
                Navigator.pop(context),
                configsViewModel.getComponents(),
                push(context, const ComponentDegreeView())
              },
            ),
            Text(LocaleKeys.config_title_others_configs.tr()),
            ListTile(
              textColor: Colors.lightBlue,
              leading: const Icon(Icons.add_link, color: Colors.lightBlue),
              title: Text(LocaleKeys.config_icalendar_url.tr()),
              onTap: () => {
                Navigator.pop(context),
                configsViewModel.initConfigs(),
                push(context, const ICalendarUrlView())
              },
            ),

          ],
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(LocaleKeys.button_cancel.tr()),
          ),
        ],
      ),
    );
  }

  _resetAllConfigs(BuildContext context, ConfigsViewModel configsViewModel) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(LocaleKeys.title_config.tr()),
        content: Text(LocaleKeys.question_delete_all_prefs.tr()),
        actions: [
          TextButton(
              onPressed: () => {
                Navigator.pop(context),
              },
              child: Text(LocaleKeys.button_cancel.tr())),
          TextButton(
              onPressed: () => {
                Navigator.pop(context),
                configsViewModel.resetAllPrefs(),
              },
              child: Text(LocaleKeys.button_ok.tr())),
        ],
      ),
    );
  }
  
  _buildConfigsList(BuildContext context, ConfigsViewModel configsViewModel) {
    if (configsViewModel.isLoading) {
      return const AppLoading();
    }
    List<CalendarPreference> lPrefs = configsViewModel.listPref;

    Color color;
    List<Widget> l = [];

    if (lPrefs.isEmpty) {
      return ListTile(
        title: Text(LocaleKeys.error_no_config_saved.tr()),
        subtitle: Text(LocaleKeys.error_add_config_with_menu.tr()),
      );
    }

    for (CalendarPreference elem in lPrefs) {
      if (elem.url == configsViewModel.selectedPref!.url) {
        color = Colors.lightGreenAccent;

        l.add(
          Card(
            child: Tooltip(
              message: elem.url,
              child: ListTile(
                title: Text(LocaleKeys.select_calendar_title.tr() + elem.title,),
                subtitle: Text(elem.toString()),
                trailing: IconButton(onPressed: () {}, icon: const Icon(Icons.star,color: Colors.yellow,)),
                tileColor: color
              )
            )
          )
        );
      }
      else {
        color = Colors.transparent;

        IconButton buttonDelete = IconButton(onPressed: () {
          showDialog<String>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: Text(elem.title),
              content: Text(LocaleKeys.question_delete_pref.tr()),
              actions: [
                TextButton(
                    onPressed: () => {
                      Navigator.pop(context),
                    },
                    child: Text(LocaleKeys.button_cancel.tr())),
                TextButton(
                    onPressed: () => {
                      Navigator.pop(context),
                      configsViewModel.deletePrefs(lPrefs.indexOf(elem))
                    },
                    child: Text(LocaleKeys.button_ok.tr())
                ),
              ],
            ),
          );
        }, icon: const Icon(Icons.delete));

        IconButton buttonSelect = IconButton(onPressed: () {
          configsViewModel.selectConfig(elem);
        }, icon: const Icon(Icons.star_border),);
        l.add(
          Card(
            child: Tooltip(
              message: elem.url,
              child: ListTile(
                tileColor: color,
                title: Text(LocaleKeys.select_calendar_title.tr() + elem.title),
                subtitle: Text(elem.toString()),
                trailing: Row(
                  mainAxisSize:MainAxisSize.min ,
                  children:[
                    buttonDelete,
                    buttonSelect
                  ]
                )
              ),
            ),
          )
        );
      }
    }

    ListView test = ListView(
      children: l,
    );


    return test;
  }
  _selectLanguageMenu(context,ConfigsViewModel configsViewModel ){
    showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
        title: const Text(LocaleKeys.title_language).tr(),
        content:
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _getLanguageUI(context, configsViewModel),

        )
    ));
  }
  _getLanguageUI(BuildContext context, ConfigsViewModel configsViewModel) {
    List<Widget> lTile = [];
    Color color = Colors.black26;
    if(context.locale == const Locale("fr")){
      color = Colors.green;
    }
    lTile.add(_getLanguageCard(context, configsViewModel, color, "🇫🇷", "fr", LocaleKeys.config_language_french));

    color = Colors.black26;
    if(context.locale == const Locale("en")){
      color = Colors.green;
    }
    lTile.add(_getLanguageCard(context, configsViewModel, color, "🇬🇧", "en", LocaleKeys.config_language_english));
    return lTile;
  }

  _getLanguageCard(BuildContext context, ConfigsViewModel configsViewModel, Color color, String flag, String locale, String lang) {
    return Card(
      child: ListTile(
        tileColor: color,
        visualDensity: VisualDensity.compact,
        leading: Text(flag),
        title: Text(lang.tr(), style: const TextStyle(fontSize: 14.0)),
        onTap: () async {
          Navigator.pop(context);
          await configsViewModel.changeLanguage(context, Locale(locale));
        },
      ),
    );
  }

  _gotoCalendar(BuildContext context,CalendarViewModel calendarViewModel) {
    calendarViewModel.initPrefs().then((value){

      if (calendarViewModel.spModele!.getSelectedPref() != null) {
        pushAndReplaceAll(context, const CalendarView());
      }
      else {
        var fToast = FToast().init(context);
        showToast(fToast, LocaleKeys.error_calendar_loading_desc.tr(),
            Colors.redAccent, const Icon(Icons.error));
      }
    });
  }
}
