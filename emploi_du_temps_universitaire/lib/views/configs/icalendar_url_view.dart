

import 'package:easy_localization/easy_localization.dart';
import 'dart:ui';
import 'package:emploi_du_temps_universitaire/translations/locale_keys.g.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../components/app_error.dart';
import '../../components/app_loading.dart';
import '../../translations/locale_keys.g.dart';

import '../../utils/navigation_utils.dart';
import '../../view_models/configs_view_model.dart';


import 'package:emploi_du_temps_universitaire/utils/navigation_utils.dart';

import 'package:provider/provider.dart';

import '../home_view.dart';






class ICalendarUrlView extends StatelessWidget {
  const ICalendarUrlView({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    ConfigsViewModel configsViewModel = context.watch<ConfigsViewModel>();

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      if(configsViewModel.alreadyIn && !configsViewModel.isLoading){
        configsViewModel.gererAlreadyIn();
        showDialog(context: context, builder: (context){
          return  AlertDialog(
            content: Text(LocaleKeys.error_config_already_register.tr()),
          );
        });
      }
    });

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      if(configsViewModel.hasError && !configsViewModel.isLoading){
        configsViewModel.gererAlreadyIn();
        showDialog(context: context, builder: (context){
          return AlertDialog(
            content: AppError(
              errorTitle: configsViewModel.error.title,
              errorDescription: configsViewModel.error.description,
            ),
          );
        });
      }
    });

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text(LocaleKeys.config_icalendar_url).tr(),
        actions: [
          IconButton(
            onPressed: () async => pushAndReplaceAll(context, const HomeView()),
            icon: const Icon(Icons.calendar_month),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child : _iCalendarUrlForm(configsViewModel,context),
      )
    );
  }



  _iCalendarUrlForm(ConfigsViewModel configsViewModel,BuildContext context)  {
    if (configsViewModel.isLoading) {
      return const AppLoading();
    }
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    String? title;
    String? url;


    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            textAlign: TextAlign.center,
            decoration:  InputDecoration(
                labelText: LocaleKeys.config_calendar_title.tr(),
                hintText: LocaleKeys.config_hint_title.tr()

            ),
            validator: (String? value) {
              if (value == null || value.isEmpty) {
                return LocaleKeys.config_error_enter_title.tr();
              } else {
                title = value;
              }
            },
          ),
          TextFormField(
            textAlign: TextAlign.center,
            decoration:  InputDecoration(
                labelText: LocaleKeys.config_icalendar_url.tr(),
                hintText: LocaleKeys.config_hint_url.tr()
            ),
            validator: (String? value) {
              if (value == null || value.trim().isEmpty) {
                return LocaleKeys.config_error_enter_url.tr();
              } else {
                url = value;
              }
            },
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Center(child:ElevatedButton(
              onPressed: () {
                // Validate will return true if the form is valid, or false if
                // the form is invalid.
                if (_formKey.currentState!.validate()) {
                  configsViewModel.checkICalendarUrl(title!, url!, context);
                }

              },
              child: Text(LocaleKeys.button_validate.tr()),
            ),
            ),
          ),
        ],
      ),
    );
  }

}