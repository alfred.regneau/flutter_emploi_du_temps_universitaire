

import 'package:easy_localization/easy_localization.dart';
import 'dart:ui';
import 'package:emploi_du_temps_universitaire/translations/locale_keys.g.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../components/app_error.dart';
import '../../components/app_loading.dart';
import '../../translations/locale_keys.g.dart';

import '../../utils/navigation_utils.dart';
import '../../view_models/configs_view_model.dart';


import 'package:emploi_du_temps_universitaire/utils/navigation_utils.dart';

import 'package:provider/provider.dart';

import '../home_view.dart';






class StudentNumberView extends StatelessWidget {
  const StudentNumberView({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    ConfigsViewModel configsViewModel = context.watch<ConfigsViewModel>();

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {if(configsViewModel.errorNum && !configsViewModel.isLoading){
      configsViewModel.gererErrorNum();
      showDialog(context: context, builder: (context){
        return  AlertDialog(
          content: Text(LocaleKeys.error_bad_student_number_desc.tr()),
        );
      });
    } });

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {if(configsViewModel.alreadyIn && !configsViewModel.isLoading){
      configsViewModel.gererAlreadyIn();
      showDialog(context: context, builder: (context){
        return  AlertDialog(
          content: Text(LocaleKeys.error_config_already_register.tr()),
        );
      });
    } });

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      if(configsViewModel.hasError && !configsViewModel.isLoading){
        configsViewModel.gererAlreadyIn();
        showDialog(context: context, builder: (context){
          return AlertDialog(
            content: AppError(
              errorTitle: configsViewModel.error.title,
              errorDescription: configsViewModel.error.description,
            ),
          );
        });
      }
    });

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text(LocaleKeys.config_student_number).tr(),
        actions: [
          IconButton(
            onPressed: () async => pushAndReplaceAll(context, const HomeView()),
            icon: const Icon(Icons.calendar_month),
          ),
        ],
      ),
      body: _studentNumberForm(configsViewModel, context));
  }



  _studentNumberForm(ConfigsViewModel configsViewModel,BuildContext context)  {
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    String? number;
    if (configsViewModel.isLoading) {
      return const AppLoading();
    }
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child :
        Column(
          children: [
           Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                   keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                  ],
                  decoration:  InputDecoration(
                    labelText: LocaleKeys.config_student_number.tr(),
                    hintText: LocaleKeys.config_hint_student_number.tr()

                  ),
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return LocaleKeys.select_calendar_no_text_error.tr();
                    } else {
                      number = value;
                    }
                  },
                ),

                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: Center(child:ElevatedButton(
                    onPressed: () {
                      // Validate will return true if the form is valid, or false if
                      // the form is invalid.
                      if (_formKey.currentState!.validate()) {
                        configsViewModel.checkStudentNumber(number!,context);
                      }
                    },
                    child: Text(LocaleKeys.button_validate.tr()),
                  ),
                  ),
                ),
              ],
            ),
          )
        ]
      ),
    );
  }

}