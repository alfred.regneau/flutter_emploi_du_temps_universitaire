

import 'package:easy_localization/easy_localization.dart';
import 'package:emploi_du_temps_universitaire/views/home_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';

import '../../components/app_error.dart';
import '../../components/app_loading.dart';
import '../../translations/locale_keys.g.dart';
import '../../utils/navigation_utils.dart';
import '../../view_models/configs_view_model.dart';

class ComponentDegreeView extends StatelessWidget {

  const ComponentDegreeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ConfigsViewModel configsViewModel = context.watch<ConfigsViewModel>();

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {if(configsViewModel.alreadyIn && !configsViewModel.isLoading){
      configsViewModel.gererAlreadyIn();
      showDialog(context: context, builder: (context){
        return  AlertDialog(
          content: Text(LocaleKeys.error_config_already_register.tr()),
        );
      });
    } });

    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      if(configsViewModel.hasError && !configsViewModel.isLoading){
        configsViewModel.gererAlreadyIn();
        showDialog(context: context, builder: (context){
          return AlertDialog(
            content: AppError(
              errorTitle: configsViewModel.error.title,
              errorDescription: configsViewModel.error.description,
            ),
          );
        });
      }
    });


    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text(LocaleKeys.config_component_degree).tr(),
          actions: [
            IconButton(
              onPressed: () async => pushAndReplaceAll(context, const HomeView()),
              icon: const Icon(Icons.calendar_month),
            ),
          ],
        ),
        body: _buildBody(context, configsViewModel));
  }

  _buildBody(BuildContext context, ConfigsViewModel configsViewModel) {
    if (configsViewModel.isLoading) {
      return const AppLoading();
    }
    return Padding(

      padding: const EdgeInsets.all(20.0),
      child :
      Column(
          children: [
            Flexible(child : _getComponentUI(configsViewModel)),
            Flexible(child : _getDegreeUI(configsViewModel)),
            _uiValidateComponent(configsViewModel,context)
          ]
      ),
    );
  }

  _getComponentUI(ConfigsViewModel configsViewModel){
    // selectCalendarModelView.getComponent();
    var listComp = configsViewModel.listComponent;
    List<Widget> lTile = [];
    int i = 0;
    Color color;
    for(var comp in listComp){
      if(i.isEven){
        color = Colors.black26;
      }
      else{
        color = Colors.white10;
      }
      if(comp == configsViewModel.actualComponent){
        color = Colors.lightGreenAccent;
      }
      i++;

      lTile.add(
          Card(child: ListTile(
            tileColor: color,
            visualDensity: VisualDensity.compact,
            title:Text(comp.item2,style: const TextStyle(fontSize: 14.0)),
            onTap: (){
              configsViewModel.setDegree(const Tuple2("",""));
              configsViewModel.setComponent(comp);
              configsViewModel.getDegrees(comp.item1);
            },

          ),

          ));

    }


    ListView lv = ListView(
      children:
      lTile,
    );

    return Container(child: lv,
      margin: const EdgeInsets.all(5.0),
      padding: const EdgeInsets.all(3.0),
      decoration: BoxDecoration(

          border: Border.all(color: Colors.blueAccent)
      ),
    );


  }

  _getDegreeUI(ConfigsViewModel configsViewModel){
    var listDegree = configsViewModel.listDegree;
    List<Widget> lTile = [];
    int i = 0;
    Color color;
    for(var degree in listDegree){
      if(i.isEven){
        color = Colors.black26;
      }
      else{
        color = Colors.white10;
      }
      if(degree == configsViewModel.acutalDegree){
        color = Colors.lightGreenAccent;
      }
      i++;
      lTile.add(
          Card(
              child: ListTile(
                tileColor: color,
                visualDensity: VisualDensity.compact,
                title: Text(degree.item2,style: const TextStyle(fontSize: 14.0)),
                onTap: (){
                  configsViewModel.setDegree(degree);
                },
                dense:false,


              )
          ));
    }


    ListView lv = ListView(

      children:
      lTile,
    );
    return Container(child: lv,
      margin: const EdgeInsets.all(5.0),
      padding: const EdgeInsets.all(3.0),
      decoration: BoxDecoration(

          border: Border.all(color: Colors.blueAccent)
      ),
    );
  }

  _uiValidateComponent(ConfigsViewModel configsViewModel,BuildContext context){
    return Column(
      children: [
        Text(LocaleKeys.select_calendar_component.tr()+configsViewModel.actualComponent.item2),
        Text(LocaleKeys.select_calendar_degree.tr()+configsViewModel.acutalDegree.item2),
        ElevatedButton(onPressed: (){
          if(configsViewModel.acutalDegree!=const Tuple2("","") && configsViewModel.actualComponent!=const Tuple2("","")){
            configsViewModel.validateDegree(context);
          }
          else{
            showDialog(context: context, builder: (context){
              return AlertDialog(
                content: Text(LocaleKeys.error_no_degree_select_desc.tr()),
              );
            });


          }

        }, child: Text(LocaleKeys.button_validate.tr())),

      ],
    );
  }

}