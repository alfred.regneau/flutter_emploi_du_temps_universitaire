

import 'package:easy_localization/easy_localization.dart';
import 'package:emploi_du_temps_universitaire/components/app_error.dart';
import 'package:emploi_du_temps_universitaire/components/app_loading.dart';
import 'package:emploi_du_temps_universitaire/models/icalendar_event_model.dart';
import 'package:emploi_du_temps_universitaire/translations/locale_keys.g.dart';
import 'package:emploi_du_temps_universitaire/utils/navigation_utils.dart';
import 'package:emploi_du_temps_universitaire/view_models/calendar_event_view_model.dart';
import 'package:emploi_du_temps_universitaire/view_models/configs_view_model.dart';
import 'package:emploi_du_temps_universitaire/views/calendar_event_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

import '../models/preferences_model.dart';
import '../view_models/calendar_compare_view_model.dart';
import 'configs_list_view.dart';
import 'home_view.dart';



class CalendarCompareView extends StatelessWidget {
  const CalendarCompareView({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    CalendarCompareViewModel calendarCompareViewModel = context.watch<CalendarCompareViewModel>();
    CalendarEventViewModel calendarEventViewModel = context.watch<CalendarEventViewModel>();
    ConfigsViewModel configsViewModel = Provider.of<ConfigsViewModel>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(LocaleKeys.title_calendar_compare.tr()),
        actions: [
          IconButton(
            onPressed: () => showDialog<String>(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                title: Text(LocaleKeys.title_config.tr()),
                content: SingleChildScrollView(
                    child: _fillConfigs(configsViewModel, calendarCompareViewModel, context)),
                actions: [
                  TextButton(
                      onPressed: () => {
                        Navigator.pop(context),
                      },
                      child: Text(LocaleKeys.button_cancel.tr())),
                ],
              ),
            ),
            icon: const Icon(Icons.add),
          ),
          IconButton(
            onPressed: () => {
              configsViewModel.initConfigs(),
              pushAndReplaceAll(context, const ConfigsListView())
            },
            icon: const Icon(Icons.settings),
          ),
          IconButton(
            onPressed: () async => pushAndReplaceAll(context, const HomeView()),
            icon: const Icon(Icons.calendar_month),
          ),
        ],
      ),
      body: Container(
          child: _buildBody(calendarCompareViewModel, context, calendarEventViewModel)
      ),
    );
  }

  _buildBody(CalendarCompareViewModel calendarCompareViewModel,
      BuildContext context, CalendarEventViewModel calendarEventViewModel) {

    if (calendarCompareViewModel.isLoading) {
      return const AppLoading();
    }
    if (calendarCompareViewModel.hasError) {
      return AppError(
        errorTitle: calendarCompareViewModel.error.title,
        errorDescription: calendarCompareViewModel.error.description,
      );
    }
    return Scaffold(
        body: SfCalendar(
          // timeSlotViewSettings: const TimeSlotViewSettings(nonWorkingDays : <int>[DateTime.sunday]),
          dataSource: calendarCompareViewModel.iCalendarDataSource,
          // by default the month appointment display mode set as Indicator, we can
          // change the display mode as appointment using the appointment display
          // mode property
          timeSlotViewSettings:
            TimeSlotViewSettings(
              nonWorkingDays : const <int>[DateTime.sunday],
              startHour: 7,
              endHour: 22,
              timeFormat: DateFormat.j(context.locale.languageCode).pattern!,
            ),
          appointmentTimeTextFormat: DateFormat.jm(context.locale.languageCode).pattern,
          resourceViewSettings: const ResourceViewSettings(showAvatar: false),
          view: CalendarView.timelineMonth,
          showDatePickerButton: true,
          allowedViews :  const <CalendarView>[
            CalendarView.timelineWorkWeek,
            CalendarView.timelineMonth,
          ],

          onTap: (CalendarTapDetails tapDetail) async {
            ICalendarEventModel? iCalendarEventModel = calendarTapped(tapDetail);
            if(iCalendarEventModel != null) {
              calendarEventViewModel.setCalendarEvent(iCalendarEventModel);
              push(context, const CalendarEventView());
            }
          },
        )
    );
  }

  ICalendarEventModel? calendarTapped(CalendarTapDetails details) {
    if (details.targetElement == CalendarElement.appointment ||
        details.targetElement == CalendarElement.agenda) {
      return details.appointments![0];
    }
    return null;
  }
  
  _fillConfigs(ConfigsViewModel configsViewModel, CalendarCompareViewModel calendarCompareViewModel, BuildContext context){
    if (configsViewModel.isLoading) {
      return const AppLoading();
    }
    List<CalendarPreference> lPrefs = configsViewModel.listPref;
    List<Widget> l = [];
    List<String> calendarNames = [];

    for (CalendarResource resource in calendarCompareViewModel.resourceColl ) {
      calendarNames.add(resource.displayName);
    }

    for (CalendarPreference elem in lPrefs) {
      if (!calendarNames.contains(elem.title.replaceAll(" ", "_"))){
        l.add(Card(child: ListTile(
            tileColor: Colors.transparent,
            title: Text(LocaleKeys.select_calendar_title.tr()+elem.title),
            subtitle: Text(elem.toString()),
            trailing: Row(mainAxisSize: MainAxisSize.min,),
            onTap: () =>
            {
              Navigator.pop(context),
              calendarCompareViewModel.addCalendar(elem),
            }
        )));
      }
    }
    if (l.isEmpty) {
      l.add(ListTile(
        title: Text(LocaleKeys.error_no_other_config_saved.tr()),
        subtitle: Text(LocaleKeys.error_add_config_with_config.tr()),
      ));
    }

    return SingleChildScrollView(
      child: Column(
        children: l,
      ),
    );
  }
}

  
