import 'package:easy_localization/easy_localization.dart';
import 'package:emploi_du_temps_universitaire/models/icalendar_event_model.dart';
import 'package:emploi_du_temps_universitaire/translations/locale_keys.g.dart';
import 'package:emploi_du_temps_universitaire/utils/navigation_utils.dart';
import 'package:emploi_du_temps_universitaire/view_models/calendar_view_model.dart';
import 'package:emploi_du_temps_universitaire/view_models/configs_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';


import '../components/app_error.dart';
import '../components/app_loading.dart';
import '../view_models/calendar_event_view_model.dart';
import 'configs_list_view.dart';
import 'home_view.dart';

class CalendarEventView extends StatelessWidget {
  const CalendarEventView({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    CalendarEventViewModel calendarEventViewModel = context.watch<CalendarEventViewModel>();
    CalendarViewModel calendarViewModel = context.watch<CalendarViewModel>();
    ConfigsViewModel configsViewModel = context.watch<ConfigsViewModel>();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(LocaleKeys.title_event.tr()),
        actions: [
          IconButton(
            onPressed: () => {
              configsViewModel.initConfigs(),
              pushAndReplaceAll(context, const ConfigsListView())
            },
            icon: const Icon(Icons.settings),
          ),
          IconButton(
            onPressed: () async => pushAndReplaceAll(context, const HomeView()),
            icon: const Icon(Icons.calendar_month),
          ),
        ],
      ),
      body: Container(
        alignment: Alignment.center,
        child: _buildBody(calendarEventViewModel, context, calendarViewModel)
      ),
    );
  }

  Widget eventDetailRow(IconData icon, String name, String value) {
    return
      ListTile(
        leading: Icon(icon, color: Colors.black),
        title: Text(name),
        subtitle: Text(value),
      );
  }

  _buildBody(CalendarEventViewModel calendarEventViewModel, BuildContext context, CalendarViewModel calendarViewModel) {
    if (calendarEventViewModel.isLoading) {
      return const AppLoading();
    }
    if (calendarEventViewModel.hasError) {
      return AppError(
        errorTitle: calendarEventViewModel.error.title,
        errorDescription: calendarEventViewModel.error.description,
      );
    }
    List<ICalendarEventModel> events = calendarEventViewModel.getIdenticalEvents(calendarEventViewModel.iCalendarEventModel, calendarViewModel.events);
    List<TableRow> tableRows = [];
    double padding = 6.0;
    tableRows.add(TableRow(children: [
      Padding(
        child: Text(LocaleKeys.event_day_start.tr()),
        padding: EdgeInsets.all(padding),
      ),
      Padding(
        padding: EdgeInsets.all(padding),
        child: Text(LocaleKeys.event_hour.tr()),
      ),
      Padding(
        padding: EdgeInsets.all(padding),
        child: Text(LocaleKeys.event_duration.tr()),
      ),
      Padding(
        padding: EdgeInsets.all(padding),
        child: Text(LocaleKeys.event_classroom.tr()),
      ),
    ]));
    for (ICalendarEventModel event in events){
      tableRows.add(TableRow(children: [
        Padding(
          padding: EdgeInsets.all(padding),
          child:  Text(event.dayStart(context.locale)),
        ),
        Padding(
          padding: EdgeInsets.all(padding),
          child: Text(event.hourStart),
        ),
        Padding(
          padding: EdgeInsets.all(padding),
          child: Text(event.duration),
        ),
        Padding(
          padding: EdgeInsets.all(padding),
          child: Text(event.location),
        ),

      ]));
    }
    return SingleChildScrollView(
      child:
        Column(
          mainAxisSize: MainAxisSize.min,
          // mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            eventDetailRow(
                Icons.topic,
                LocaleKeys.event_summary.tr(),
                calendarEventViewModel.iCalendarEventModel.summary
            ),
            eventDetailRow(
                Icons.location_on,
                LocaleKeys.event_location.tr(),
                calendarEventViewModel.iCalendarEventModel.location
            ),
            eventDetailRow(
                Icons.today,
                LocaleKeys.event_day_start.tr(),
                calendarEventViewModel.iCalendarEventModel.dayStart(context.locale)
            ),
            eventDetailRow(
                Icons.access_time,
                LocaleKeys.event_hour.tr(),
                calendarEventViewModel.iCalendarEventModel.hourStart
            ),
            eventDetailRow(
                Icons.timer,
                LocaleKeys.event_duration.tr(),
                calendarEventViewModel.iCalendarEventModel.duration
            ),
            eventDetailRow(
                Icons.description,
                LocaleKeys.event_description.tr(),
                calendarEventViewModel.iCalendarEventModel.description
            ),

            ListTile(
              leading: Icon(Icons.list, color: Colors.black),
              title: Text(LocaleKeys.event_slots_list.tr()),
              subtitle: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child:
                Table(
                  defaultColumnWidth: const IntrinsicColumnWidth(),
                  children: tableRows,
                  border: TableBorder.all(),
                ),
              ),
            )
            // _ui(usersViewModel),
            // Padding(
            //   padding: const EdgeInsets.all(5.0),
            //   child:
            //     Column(
            //       mainAxisAlignment: MainAxisAlignment.start,
            //       crossAxisAlignment: CrossAxisAlignment.start,
            //       children: [
            //         Padding(
            //           padding: const EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 10.0),
            //           child: Text(LocaleKeys.event_slots_list.tr(), textScaleFactor: 1.2)),
            //         SingleChildScrollView(
            //           scrollDirection: Axis.horizontal,
            //           child:
            //             Table(
            //               defaultColumnWidth: const IntrinsicColumnWidth(),
            //               children: tableRows,
            //               border: TableBorder.all(),
            //             ),
            //         )
            //     ],
              // )
            // )
          ],
        )
    );
  }
}