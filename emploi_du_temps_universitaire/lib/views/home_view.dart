import 'package:emploi_du_temps_universitaire/view_models/configs_view_model.dart';
import 'package:emploi_du_temps_universitaire/view_models/calendar_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../components/app_loading.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    ConfigsViewModel configsViewModel = context.watch<ConfigsViewModel>();
    CalendarViewModel calendarViewModel = Provider.of<CalendarViewModel>(context);


     checkLinkAndNavigate(context).then((res) {
       if(res==false){
         configsViewModel.initConfigs();

         Navigator.popAndPushNamed( context, '/configs');
       }
       else{
         calendarViewModel.initPrefs();
         Navigator.popAndPushNamed(context, '/calendar');
       }
     });

    return const AppLoading();
  }


  Future checkLinkAndNavigate(BuildContext context) async {
    bool status;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String? userdata = preferences.getString("selectedPref");
    if (userdata != null) {
      status = true;
    } else {
      status = false;
    }
    return status;
  }
}