

import 'package:easy_localization/easy_localization.dart';
import 'package:emploi_du_temps_universitaire/components/app_error.dart';
import 'package:emploi_du_temps_universitaire/components/app_loading.dart';
import 'package:emploi_du_temps_universitaire/models/icalendar_event_model.dart';
import 'package:emploi_du_temps_universitaire/translations/locale_keys.g.dart';
import 'package:emploi_du_temps_universitaire/utils/navigation_utils.dart';
import 'package:emploi_du_temps_universitaire/view_models/calendar_event_view_model.dart';
import 'package:emploi_du_temps_universitaire/view_models/calendar_view_model.dart';
import 'package:emploi_du_temps_universitaire/view_models/configs_view_model.dart';
import 'package:emploi_du_temps_universitaire/views/calendar_event_view.dart';
import 'package:emploi_du_temps_universitaire/views/calendar_compare_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart' as sf;

import '../view_models/calendar_compare_view_model.dart';
import 'configs_list_view.dart';

class CalendarView extends StatelessWidget {
  const CalendarView({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    CalendarViewModel calendarViewModel = context.watch<CalendarViewModel>();
    CalendarCompareViewModel calendarCompareViewModel = context.watch<CalendarCompareViewModel>();
    CalendarEventViewModel calendarEventViewModel = context.watch<CalendarEventViewModel>();
    ConfigsViewModel configsViewModel = context.watch<ConfigsViewModel>();



      return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(LocaleKeys.title_calendar.tr()),
        actions: [
          IconButton(
            onPressed: () => {
              configsViewModel.initConfigs(),
              pushAndReplaceAll(context, const ConfigsListView())
            },
            icon: const Icon(Icons.settings),
          ),
          IconButton(
            onPressed: () => {
              calendarCompareViewModel.initPrefs(),
              configsViewModel.initConfigs(),
              push(context, const CalendarCompareView()),
            },
            icon: const Icon(Icons.compare_arrows),
          ),
        ],
      ),
      body: Container(
          child: _buildBody(calendarViewModel, context, calendarEventViewModel)
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: (){
      //     _gotoList(context,configsViewModel);
      //   },
      //   child: const Icon(Icons.settings),
      // ),
    );
    }



  _gotoList(BuildContext context,ConfigsViewModel configsViewModel){
    configsViewModel.initConfigs();
    pushAndReplaceAll(context, const ConfigsListView());
  }

  _buildBody(CalendarViewModel calendarViewModel,
      BuildContext context, CalendarEventViewModel calendarEventViewModel) {





    if (calendarViewModel.isLoading) {
      return const AppLoading();
    }
    if (calendarViewModel.hasError) {
      return AppError(
        errorTitle: calendarViewModel.error.title,
        errorDescription: calendarViewModel.error.description,
      );
    }
    return Scaffold(
        body: sf.SfCalendar(
          timeSlotViewSettings:
            sf.TimeSlotViewSettings(
              nonWorkingDays : const <int>[DateTime.sunday],
              startHour: 7,
              endHour: 22,
              timeFormat: DateFormat.j(context.locale.languageCode).pattern!,
            ),
          appointmentTimeTextFormat: DateFormat.jm(context.locale.languageCode).pattern,
          view: sf.CalendarView.workWeek,
          showDatePickerButton: true,
          allowedViews :  const <sf.CalendarView>[
            sf.CalendarView.day,
            sf.CalendarView.workWeek,
            sf.CalendarView.month,
            sf.CalendarView.timelineMonth,
            sf.CalendarView.schedule,
            ],
          dataSource: calendarViewModel.iCalendarDataSource,
          // by default the month appointment display mode set as Indicator, we can
          // change the display mode as appointment using the appointment display
          // mode property
          onTap: (sf.CalendarTapDetails tapDetail) async {
            ICalendarEventModel? iCalendarEventModel = calendarTapped(tapDetail);
            if(iCalendarEventModel != null) {
              calendarEventViewModel.setCalendarEvent(iCalendarEventModel);
              push(context, const CalendarEventView());
            }
          },
        )
    );
  }

  ICalendarEventModel? calendarTapped(sf.CalendarTapDetails details) {
    if (details.targetElement == sf.CalendarElement.appointment ||
        details.targetElement == sf.CalendarElement.agenda) {
      return details.appointments![0];
    }
    return null;
  }

}
