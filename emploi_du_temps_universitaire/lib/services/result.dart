import 'package:http/http.dart' as http;

class Result {}

class Success extends Result {
  http.Response response;
  Success({required this.response});
}

class Failure extends Result {
  Error error;

  Failure({required this.error});
}


class Error {
  String title;
  String description;

  Error({required this.title, required this.description});
}