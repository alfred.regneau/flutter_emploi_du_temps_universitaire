import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:emploi_du_temps_universitaire/translations/locale_keys.g.dart';
import 'package:http/http.dart' as http;

import 'result.dart';

const serverRequestUrl = "https://www.univ-orleans.fr/EDTWeb/edt";

class RequestService {

  static Result checkResponse(http.Response response) {
    switch (response.statusCode) {
      case HttpStatus.ok:
      // case HttpStatus.created:
        return Success(response: response);
      case HttpStatus.badRequest:
        return Failure(error: Error(title: LocaleKeys.error_bad_request.tr(), description: response.reasonPhrase!));
      case HttpStatus.unauthorized:
      case HttpStatus.forbidden:
        return Failure(error: Error(title: LocaleKeys.error_unauthorized.tr(), description: response.reasonPhrase!));
      default:
        return Failure(error: Error(title: LocaleKeys.error_status_code.tr(namedArgs: {"code": response.statusCode.toString()}), description: response.reasonPhrase!));
    }
  }
  

  static Future<Result> httpGetRequest(Uri uri) async {
    try {
      return checkResponse(await http.get(uri));
    } on HttpException catch(e) {
      return Failure(error: Error(title: LocaleKeys.error_no_connection.tr(), description: e.toString()));
    } on SocketException catch(e) {
      return Failure(error: Error(title: LocaleKeys.error_no_connection.tr(), description: e.toString()));
    } on FormatException catch(e) {
      return Failure(error: Error(title: LocaleKeys.error_invalid_format.tr(), description: e.toString()));
    } catch (e) {
      return Failure(error: Error(title: LocaleKeys.error_unknown.tr(), description: e.toString()));
    }
  }
  

  // Meme chose qu'au dessus mais version post
  static Future<Result> httpPostRequest(/* uri + parameters.. */) async {
    throw UnimplementedError();
  }
  

  // Récupération du fichier iCalendar
  static Future<Result> fetchICalendar(String iCalUrl) async {
      return await httpGetRequest(Uri.parse(iCalUrl));
  }



  // Renvoie la page de l'emploi du temps contenant le lien iCalendar (à extraire ensuite)
  static Future<Result> validateStudentNumber(int studentNumber) async {
    String url = "https://www.univ-orleans.fr/EDTWeb/edt?action=displayWeeksPeople&person="+studentNumber.toString();
   return await RequestService.httpGetRequest(Uri.parse(url));

  }


  // Renvoie la page contenant la liste des composantes (à extraire ensuite)
  static Future<Result> fetchComponents() async {
    String url = "https://www.univ-orleans.fr/EDTWeb/edt";
    return await RequestService.httpGetRequest(Uri.parse(url));
  }


  // Renvoie la page contenant la liste des filières correspondant à la composante (à extraire ensuite)
  static Future<Result> fetchDegrees(String component) async {
    // POST serverRequestUrl avec les paramètres nécessaires
    String url = "https://www.univ-orleans.fr/EDTWeb/edt?action=displayFilieres&project=2021-2022&composantes="+component;
    return await RequestService.httpGetRequest(Uri.parse(url));
  }


  // Renvoie la page de l'emploi du temps contenant le lien ical (à extraire ensuite)
  static Future<Result> validateComponentDegree(String component, String degree) async {
    // POST serverRequestUrl avec les paramètres nécessaires
    String url = "https://www.univ-orleans.fr/EDTWeb/edt?action=displayFilieres&project=2021-2022&composantes="+component+"&filieres="+degree;
    return await RequestService.httpGetRequest(Uri.parse(url));
  }

}
