import 'package:flutter/material.dart';

class AppError extends StatelessWidget {
  final String errorTitle;
  final String errorDescription;
  const AppError({Key? key, required this.errorTitle, required this.errorDescription}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [Container(
            padding: const EdgeInsets.all(10.0),
            child: ListTile(
              leading: const Icon(Icons.error, color: Colors.red),
              title: Text(
                  errorTitle,
                  style: const TextStyle(color: Colors.red, fontSize: 18.0)),
              subtitle: Text(errorDescription),
            ),
          ),
      ]
    );

  }
}
