import 'package:emploi_du_temps_universitaire/translations/codegen_loader.g.dart';
import 'package:emploi_du_temps_universitaire/view_models/calendar_compare_view_model.dart';
import 'package:emploi_du_temps_universitaire/view_models/calendar_view_model.dart';
import 'package:emploi_du_temps_universitaire/view_models/configs_view_model.dart';
import 'package:emploi_du_temps_universitaire/view_models/calendar_event_view_model.dart';
import 'package:emploi_du_temps_universitaire/views/calendar_view.dart';
import 'package:emploi_du_temps_universitaire/views/configs_list_view.dart';
import 'package:emploi_du_temps_universitaire/views/home_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();


  runApp(
    EasyLocalization(
      supportedLocales: const [
        Locale('fr'),
        Locale('en')
      ],
      path: 'assets/translations',
      fallbackLocale: const Locale('fr'),
      assetLoader: const CodegenLoader(),
      child: const MyApp(),
    )
  );
}


class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //SharedPreferences.setMockInitialValues({});

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => CalendarEventViewModel()),
        ChangeNotifierProvider(create: (_) => ConfigsViewModel()),
        ChangeNotifierProvider(create: (_) => CalendarViewModel()),
        ChangeNotifierProvider(create: (_) => CalendarCompareViewModel()),
      ],
      child: MaterialApp(
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          brightness: Brightness.light,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: const HomeView(),
        routes: {
          '/calendar':(context)=>const CalendarView(),
          '/configs':(context)=>const ConfigsListView()
        },
      ),
    );
  }

}
