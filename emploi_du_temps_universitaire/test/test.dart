// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:emploi_du_temps_universitaire/services/request_service.dart';
import 'package:emploi_du_temps_universitaire/services/result.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  
  
  group("Request",(){
    test("Recupere une page avec un num etudiant valide",() async {
      Result res = await RequestService.validateStudentNumber(2181271);
      if(res is Success){
        //on regarde directement si un lien est disponible car le site de la fac ne met pas d erreur si les on rentre des fausse informations
        var test = res.response.body.contains("http://www.univ-orleans.fr/EDTWeb/export?project");
        expect(test,true);
      }

    });

    test("Recupere une page avec un num etudiant invalide",() async {
      Result res = await RequestService.validateStudentNumber(0);
      if(res is Success){
        //on regarde directement si un lien est disponible car le site de la fac ne met pas d erreur si les on rentre des fausse informations
        var test = res.response.body.contains("http://www.univ-orleans.fr/EDTWeb/export?project");
        expect(test,false);
      }

    });

    test("Lien iCalendar faux",() async {
      Result res = await RequestService.fetchICalendar("PasUnLieniCalUrl");
        //on attend une erreur si le lien n'est pas un lien valide
        expect(res.runtimeType,Failure);


    });

    test("Lien iCalendar bon",() async {
      Result res = await RequestService.fetchICalendar("http://www.univ-orleans.fr/EDTWeb/export?project=2021-2022&resources=35838&type=ical");
      //on attend un succes si le lien est valide .
      expect(res.runtimeType,Success);

    });

    test("BonDegreeEtComponent",()async{
      Result res = await RequestService.validateComponentDegree("SCT", "751");
      if(res is Success){
        //on regarde directement si un lien est disponible car le site de la fac ne met pas d erreur si les on rentre des fausse informations
        var test = res.response.body.contains("http://www.univ-orleans.fr/EDTWeb/export?project");
        expect(test,true);
      }
    });

    test("MauvaisDegreeEtComponent",()async{
      Result res = await RequestService.validateComponentDegree("SCdazdzadT", "azdazdaz");
      if(res is Success){
        //on regarde directement si un lien est disponible car le site de la fac ne met pas d erreur si les on rentre des fausse informations
        var test = res.response.body.contains("http://www.univ-orleans.fr/EDTWeb/export?project");
        expect(test,false);
      }
    });

    test("MauvaisDegreeEtBonneComponent",()async{
      Result res = await RequestService.validateComponentDegree("SCT", "azdazdaz");
      //dans le cas d une bonne component mais d'un mauvaise degree le site retourne une erreur
     expect(res.runtimeType,Failure);
    });





  });

}
